// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'
import swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';


export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.prototype.$swiper = swiper
  head.script.push({ src: 'https://kit.fontawesome.com/a076d05399.js' })
  head.script.push({ src: 'https://kit.fontawesome.com/cbb910fe1f.js' })
  head.script.push({ src: 'https://unpkg.com/swiper/swiper-bundle.min.js' })
  head.link.push({
    rel: 'stylesheet',
    href: 'https://unpkg.com/swiper/swiper-bundle.min.css'
  })
   head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Barlow'
  })
  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Work+Sans'
  })

  Vue.component('Layout', DefaultLayout)
}
