module.exports = function (api) {
  api.loadSource(async actions => {
    const { data } = await axios.get('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2F%40mr.arifinsyah')

    const collection = actions.addCollection('MediumPost')

    for (const item of data) {
      collection.addNode({
        items: item.items,
        title: item.title,
        link: item.link,
        thumbnail: item.thumbnail,
        pubDate: item.pubDate,
      })
    }
  })
}


module.exports = function (api) {
  api.loadSource(async actions => {
    const Blogs = require('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2F%40mr.arifinsyah');

    const collection = actions.addCollection({
      typeName: 'MediumPost'
    })

    for (const item of Blogs) {
      collection.addNode({
        items: item.items,
        title: item.title,
        link: item.link,
        thumbnail: item.thumbnail,
        pubDate: item.pubDate,
      })
    }
  })
}
