// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Gridsome',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'articles/**/*.md',
        typeName: 'Articles',
        remark: {}
      },
      refs: {
        tags: {
          typeName: "Tag",
          route: "/tag/:id",
          create: true
        }
      }
    },
    {
      use: '@zefman/gridsome-source-instagram',
      options: {
        username: 'inddesitb', // Instagram username
        typeName: 'InstagramPhoto' // The GraphQL type you want the photos to be added under. Defaults to InstagramPhoto
      }
    },
  ],
  transformers: {
    remark: {}
  }
}
