# Website INDDES
### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Create a Gridsome project

1. `gridsome create websiteinddes git@gitlab.com:rivianp/inddes-testing.git` to install Website INDDES on your local 
2. `cd websiteinddes` to open the folder
3. `gridsome develop` to start a local dev server at `http://localhost:8080`
4. Happy coding ndes! 🎉🙌
